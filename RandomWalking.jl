
using Distributions

#(x^2 + x) (x0 = -1)
#(x^3 + 6 * x^2 - 7) (x0 = -1)
#(x^3 +6x^2 + 9x - 3) (x0 = -1,5)

function f(x::Float64)::Float64 
    return (x^3 +6x^2 + 9x - 3)
end

function randomWalk()
    x = 0.0
    x0 = 0.25
    y = 0.0
    y0 = f(x0)
    step = sqrt(2)
    e = 0.1
    MAX = 1000
    i = 1
    while (true)
        r = rand(Uniform(-1, 1)) * step;
        x = x0 + r
        y = f(x)
        if(y < y0)
            y0 = y
            x0 = x
            i = 1
        else
            if( i <= MAX)
                i = i + 1
            else
                step = step / 2
                if(step <= e)
                    break;
                end
            end
        end
    end
    println("X: ", x)
    println("Y: ", y)
end
    

randomWalk()
